# ForestCoverType

This project is an implementation of the Forest Cover Type problem, which uses a simple logistic regression model to try to predict the forest cover type with a list of given attributes.
For a detailed description of the input data used by the neural network, visit: [covtype data](http://archive.ics.uci.edu/ml/machine-learning-databases/covtype/) and open covtype.info
## Getting Started
### Prerequisites
To run this project you need to install the python3.6 interpreter.
Go to [python's](https://www.python.org/) web page.
Note: I highly recommend to install the python interpreter using the Anaconda package, because it contains the whole packages needed to run this project. Go to [anaconda's](https://www.anaconda.com/download/) web page.

If you decided to install python3.6 standard interpreter you need to install pandas and scipy packages. After installing the interpreter run the following commands:
```
pip3.6 install pandas scipy matplotlib
```
or 
```
pip install pandas scipy matplotlib
```
Note: you may need administrator permissions.

### Installing
Start by downloading the package from [developer's](https://bitbucket.org/dfonnegra/forestcovertype/) bitbucket repository or clone it using the command:
```
git clone https://dfonnegra@bitbucket.org/dfonnegra/forestcovertype.git
```
After downloading the package from the terminal or cmd (On Windows) run:
```
cd /path/to/project/
```
Next, verify which python interpreter is being used:
On Windows
```
where python
```
On Linux
```
which python3.6
```
It must return the folder of python interpeter. If you installed anaconda it must be
```
/some_path/anaconda3/some_path/
```
Finally run
```
python3.6 setup.py install
```
or
```
python setup.py install
```
If there is any error, run:
```
/path/to/python_interpreter/python3.6 setup.py install
```
If everything runs correctly you should have a /build/ folder in the project's folder.

### Running the program
First, from the command line get into the /path/to/project/build/lib/ folder.
The program have 2 runnable modules: /data_set_analysis/data_histogram.py, /neural_network/main_nn.py.
To run any of them get into the folder of the module from the command line and run:
```
python3.6 name_of_module.py
```
or 
```
python name_of_module.py
```

### Data histogram module.
This module shows histograms for each of the attributes (Except for the Soil type) to see the distribution of the data in the dataset.
Some observations here:
- If you verify the Cover Type histogram, it is showing a highly concentrated data in the type 2 cover type.
- Most of the attributes have a gaussian like distribution.

### Neural Network Main Module
This is the most important module. After this module, it'll start to depurate the data and train the neural network. This may take one or two minutes.
When the neural network training finishes you have 3 options:
- Insert a file path which contains data attributes in this format ([covtype data](http://archive.ics.uci.edu/ml/machine-learning-databases/covtype/)). It may or may not contain the Cover Type column.
 - If it contains the Cover Type column, the following lines will have the format: ```Real: Value Prediction: Value.``` And the last line will have the accuracy of the prediction.
 - If it doesn't contain the Cover Type column, the following lines will have only the Prediction value.
- Write "sample". This command takes a random subset of size 30 from the test set of the neural network and shows the results of the prediction in the format: ```Real: Value Prediction: Value.``` And again, the last line will have the accuracy of the prediction. Note: The accuracy may vary because the sample taken is random, so if you run sample several times you should see accuracies around the value given when the neural network was trained.
- Write "exit" to finish the program.

### Running tests
To run the tests, follow the instructions of the "Running the program" section and run the /neural_network_tests/NeuralNetwork_Test.py module.

## Solving Errors
- If the program doesn't run because it's missing pandas, scipy or matplotlib modules, consider running the command described in the "Prerequisites" section.
- If you have errors like MemoryError or _tk library, consider using the anaconda's interpreter. Verify that when running ```where python``` on Windows or ```which python3.6``` on linux the path is set to the anaconda's path. If it is not, run ```/path_to_anaconda3/python module.py```. Note: If you are going to run it in this way, you have to install it again running ```/path_to_anaconda3/python setup.py install```. After this everything should work!

## Annotations
- A simple logistic regression model was first implemented because it is the simplest one for the categorization predictions. After the implementation was finished, I realized that the accuracy was around 71%, and the literature for more elaborated neural networks report accuracies around 78%. So I considered this implementation was good enough to be the first one.
- Due to the high amount of data, the neural network isn't trained with regularization parameter (Overfitting is not probable). Altough, the implementation contains a method to train the neural network with regularization parameter if a smalles dataset is going to be used.

## Future implementations
This was a simple implementation of the Forest Cover Type module. Future implementations could add the following:
- Add layers to the neural network in order to improve implementation.
- A principal component analysis to reduce computation time reducing the number of attributes. For the current dataset for example, the soil type one was always present, so its value was always 1.0.









