from distutils.core import setup


setup(name='ForestCoverType', version='1.0', author='Daniel Fonnegra',
      packages=['data_set', 'data_set_analysis', 'neural_network', 'neural_network_test', 'test_datasets'],
      package_data={'data_set': ['./*'], 'test_datasets' : ['./*']})