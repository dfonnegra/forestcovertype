import unittest
from data_set_analysis.DataDepuration import DataDepuration
from neural_network.NeuralNetwork import NeuralNetwork
import os
import numpy


class NeuralNetwork_Test(unittest.TestCase):

    def test_failing(self):
        self.assertRaises(Exception, self.fail)

    def test_nn_sets_sizes(self):
        dataset_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) \
                        +"/test_datasets/nn_test_dataset1.data"
        data_depuration = DataDepuration(dataset_location)
        neural_net = NeuralNetwork(attributes_dataframe=data_depuration.forest_attributes_data_frame(),
                                   output_series = data_depuration.forest_cover_type_series(), n_labels=7)
        self.assertEqual(neural_net.train_set.shape[0], neural_net.n_train)
        self.assertEqual(neural_net.cross_set.shape[0], neural_net.n_cross)
        self.assertEqual(neural_net.test_set.shape[0], neural_net.n_test)
        self.assertEqual(neural_net.train_set.shape[1], 54)
        self.assertEqual(neural_net.cross_set.shape[1], 54)
        self.assertEqual(neural_net.test_set.shape[1], 54)
        self.assertEqual(neural_net.output_train.shape[0], neural_net.n_train)
        self.assertEqual(neural_net.output_cross.shape[0], neural_net.n_cross)
        self.assertEqual(neural_net.output_test.shape[0], neural_net.n_test)
        self.assertEqual(neural_net.output_train.shape[1], 7)
        self.assertEqual(neural_net.output_cross.shape[1], 7)
        self.assertEqual(neural_net.output_test.shape[1], 7)

    def test_nn_normalize_data(self):
        dataset_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) \
                           + "/test_datasets/nn_test_dataset1.data"
        data_depuration = DataDepuration(dataset_location)
        normalized_data = NeuralNetwork.normalize_data(data_depuration.forest_attributes_data_frame())
        for value in normalized_data.mean():
            self.assertTrue(numpy.abs(value) < 1E-10)
        for value in normalized_data.max()-normalized_data.min():
            if value == 0.0: continue
            self.assertTrue(numpy.abs(value-1.0) < 1E-10)
        #neural_net = NeuralNetwork(attributes_dataframe=data_depuration.forest_attributes_data_frame(),
        #                           output_series=data_depuration.forest_cover_type_series(), n_labels=7)


    def test_sigmoid_function(self):
        self.assertAlmostEqual(NeuralNetwork.sigmoid_function(2.0), 0.8807970779778823, delta=1E-6)
        test_array = numpy.array([-5.0, 0.2, 1.1])
        self.assertAlmostEqual(NeuralNetwork.sigmoid_function(test_array)[0],
                               0.00669285, delta=1E-6)
        self.assertAlmostEqual(NeuralNetwork.sigmoid_function(test_array)[1],
                               0.54983399, delta=1E-6)
        self.assertAlmostEqual(NeuralNetwork.sigmoid_function(test_array)[2],
                               0.75026010, delta=1E-6)
        test_array = numpy.asmatrix([[-5.0, 0.0, -100.0], [2.0, 1.5, 5.5], [100.0, 0.01, 1.2]])
        result = NeuralNetwork.sigmoid_function(test_array)
        expected_result = numpy.asmatrix([[0.006692850, 0.5, 0.0], [0.88079707, 0.81757447, 0.99592986]
                                             , [1.000, 0.50249997, 0.76852478]])
        for i in range(3):
            for j in range(3):
                self.assertAlmostEqual(result[i, j], expected_result[i, j], delta=1E-6)

    def test_cost_function(self):
        X = numpy.array([[2.0]])
        y = numpy.array([[1.0]])
        theta = numpy.array([[4.0], [-5.0]])
        self.assertAlmostEqual(NeuralNetwork.cost_function(X=X, y=y, theta=theta),
                               6.00247568513773, delta=1E-6)
        X = numpy.array([[2.0, 3.0], [-1.8, 1.4]])
        y = numpy.array([[1.0], [0.0]])
        theta = numpy.array([[4.0], [-5.0], [2.6]])
        self.assertAlmostEqual(NeuralNetwork.cost_function(X=X, y=y, theta=theta),
                               8.39648883500426, delta=1E-6)
        X = numpy.array([[1.0, -3.0], [-1.8, 1.4]])
        y = numpy.array([[0.0, 1.0], [1.0, 0.0]])
        theta = numpy.array([[4.0, 2.1], [-5.0, 1.1], [8.2, 2.6]])
        self.assertAlmostEqual(NeuralNetwork.cost_function(X, y, theta),
                               4.196509230834463, delta=1E-6)
        X = numpy.array([[1.0, -3.0], [-1.8, 1.4]])
        y = numpy.array([[0.0, 1.0], [1.0, 0.0]])
        theta = numpy.array([[4.0, 2.1], [-5.0, 1.1], [8.2, 2.6]])
        self.assertAlmostEqual(NeuralNetwork.cost_function(X, y, theta, reg_parameter=0.5),
                               5.744009230834464, delta=1E-6)

    def test_grad_function(self):
        X = numpy.array([[1.0, -3.0], [-1.8, 1.4]])
        y = numpy.array([[0.0, 1.0], [1.0, 0.0]])
        theta = numpy.array([4.0, 2.1, -5.0, 1.1, 8.2, 2.6])
        real_grad = NeuralNetwork.cost_function_grad(X = X, y = y, theta=theta)
        #The estimate of the gradient using the cost function and the real gradient
        #Must be alike.
        for i in range(6):
            epsilon = numpy.zeros(theta.shape)
            epsilon[i] = 0.00001
            aux_theta = theta + epsilon
            grad_estimation = (NeuralNetwork.cost_function(X, y, aux_theta) -
                               NeuralNetwork.cost_function(X, y, theta)) / 0.00001
            self.assertAlmostEqual(real_grad[i], grad_estimation, delta=1E-5)

        real_grad = NeuralNetwork.cost_function_grad(X=X, y=y, theta=theta, reg_parameter=0.01)
        # The estimate of the gradient using the cost function and the real gradient
        # Must be alike.
        for i in range(6):
            epsilon = numpy.zeros(theta.shape)
            epsilon[i] = 0.00001
            aux_theta = theta + epsilon
            grad_estimation = (NeuralNetwork.cost_function(X, y, aux_theta, 0.01) -
                               NeuralNetwork.cost_function(X, y, theta, 0.01)) / 0.00001
            self.assertAlmostEqual(real_grad[i], grad_estimation, delta=1E-5)

    def test_nn_train(self):
        dataset_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) \
                           + "/test_datasets/nn_test_dataset2.data"
        data_depuration = DataDepuration(dataset_location)
        att_df = data_depuration.forest_attributes_data_frame();
        out_df = data_depuration.forest_cover_type_series();

        neural_net = NeuralNetwork(attributes_dataframe=att_df,
                                   output_series=out_df, n_labels=7)
        neural_net.train_nn()
        accuracy = neural_net.nn_accuracy(neural_net.test_set.as_matrix(), neural_net.output_test.as_matrix())
        self.assertTrue(accuracy > 0.6, "Implementations must always get an accuracy greater"
                                        "than 60% because a simple neural network without hidden"
                                        "layers returns accuracies of 62%")
        neural_net.train_regularized_nn()
        accuracy = neural_net.nn_accuracy(X=neural_net.test_set.as_matrix(),
                                          y=neural_net.output_test.as_matrix())
        self.assertTrue(accuracy > 0.6, "Implementations must always get an accuracy greater"
                                        "than 60% because a simple neural network without hidden"
                                        "layers returns accuracies of 62%")

if __name__ == '__main__':
    unittest.main()
