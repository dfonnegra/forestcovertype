from data_set_analysis.DataDepuration import DataDepuration
import random
import numpy
import pandas
from scipy.optimize import minimize

class NeuralNetwork:

    n_data = 0
    n_train = 0
    n_test = 0
    n_cross = 0
    theta = None

    def __init__(self, attributes_dataframe, output_series, n_labels):
        attributes_dataframe = self.normalize_data(attributes_dataframe)
        self.n_data = attributes_dataframe.shape[0]
        self.n_train = int(3.0*self.n_data/5)
        self.n_cross = int(1.0*self.n_data/5)
        self.n_test = self.n_data - self.n_train - self.n_cross
        self.reg_parameter = 0.0

        #Transforms the output series into an output dataframe where the i-th column
        #is 1.0 if y = i
        output_dataframe = numpy.zeros(shape=(len(output_series), n_labels))
        row_count = 0
        ones_count = 0
        for label in output_series:
            if label == 1.0:
                ones_count+=1
            output_dataframe[row_count, int(label)-1] = 1.0
            row_count += 1
        output_dataframe = pandas.DataFrame(data=output_dataframe, columns=list(range(n_labels)))
        #Randomly distributes the data set to the training set, the cross validation set
        #And the test set
        indexes_range = list(range(self.n_data))
        random.shuffle(indexes_range)
        self.train_set = attributes_dataframe.iloc[indexes_range[:self.n_train]]
        self.cross_set = attributes_dataframe.iloc[indexes_range[self.n_train:self.n_train + self.n_cross]]
        self.test_set = attributes_dataframe.iloc[indexes_range[self.n_train + self.n_cross:]]

        self.output_train = output_dataframe.iloc[indexes_range[:self.n_train]]
        self.output_cross = output_dataframe.iloc[indexes_range[self.n_train:self.n_train + self.n_cross]]
        self.output_test = output_dataframe.iloc[indexes_range[self.n_train + self.n_cross:]]

    def train_nn(self):
        X = self.train_set.as_matrix()
        y = self.output_train.as_matrix()
        print("Training Neural Network...")
        self.theta = self.__minimize_cost(X, y, self.reg_parameter)
        print("Training finished...")

    def train_regularized_nn(self):
        self.__best_reg_param()
        self.train_nn()

    @staticmethod
    def __minimize_cost(X, y, reg_param = 0.0):
        '''
        Returns the value of the parameter theta such that cost_function is minimized
        :param X: Data set of size mxn, containing n attributes in the columns and m observations
            in the rows
        :param y: Output's of the dataset of size mx(n_labels), containing m observations in the rows and
            n_labels labels in the columns.
        :param reg_parameter: Regularization parameter value
        :return: Value of the parameter theta such that the cost_function is minimized
        '''
        param = reg_param
        n = X.shape[1]
        n_labels = y.shape[1]
        min_result = minimize(fun=lambda theta: NeuralNetwork.cost_function(X, y, theta, param),
                              x0=numpy.zeros((n + 1) * n_labels),
                              jac=lambda theta: NeuralNetwork.cost_function_grad(X, y, theta, param),
                              options={'maxiter': 100, 'disp': False})
        return min_result.x.reshape(n+1, n_labels)

    def nn_prediction(self, X):
        '''
        Returns the prediction of the trained neural network.
        :param X: Data set of size mxn, containing n attributes in the columns and m observations
        in the rows
        :return: 1D-array of predictions for each observation of the data_set.
        '''
        #Normalizes the data.
        if self.theta is None:
            self.train_nn()
        m = X.shape[0]
        X = numpy.column_stack((numpy.ones(m), X))

        h = self.sigmoid_function(X.dot(self.theta))
        prediction = numpy.argmax(h, axis=1)+1.0
        return prediction

    def __best_reg_param(self):
        '''
        Uses the cross-validation set to find the best value of the regularization parameter which
        gives the higher accuracy.
        '''
        reg_param_list = [0.001, 0.01, 0.1, 1, 10.0]
        X_cross = self.cross_set.as_matrix()
        y_cross = self.output_cross.as_matrix()
        best_reg_param = 0.0
        self.reg_parameter = best_reg_param
        self.train_nn()
        best_accuracy = self.nn_accuracy(X_cross, y_cross)
        print("Calculating regularization param...")
        for param in reg_param_list:
            self.reg_parameter = param
            self.train_nn()
            accuracy = self.nn_accuracy(X_cross, y_cross)
            if accuracy > best_accuracy:
                best_reg_param = param
                best_accuracy = accuracy
            print("Accuracy for param = ", param, "is", "{0:.1f}%".format(accuracy*100))
        print("Regularization param calculated:", best_reg_param)
        self.reg_parameter = best_reg_param

    def nn_accuracy(self, X, y):
        '''
        :param X: Data set of size mxn, containing n attributes in the columns and m observations
            in the rows
        :param y: Output's of the dataset of size mx(n_labels), containing m observations in the rows and
            n_labels labels in the columns.
        :return: Accuracy of the neural network for the given dataset
        '''
        predict = self.nn_prediction(X)
        if len(y.shape) > 1:
            real_y = numpy.argmax(y, axis=1)+1.0
        else:
            real_y = y
        accuracy = (numpy.equal(predict, real_y)).astype(numpy.int)
        accuracy = sum(accuracy)/len(accuracy)
        return accuracy

    @staticmethod
    def normalize_data(dataframe):
        '''
        Normalizes the columns of the dataframe with the equation z = (x-u)/(x_min - x_max).
        If there is a column such that x_min = x_max then the normalization is z = (x-u)
        (Which obviously will always equal to zero)
        :param dataframe: Pandas dataframe containing the data
        :return: A pandas dataframe with the data normalized.
        '''
        normalized_df = pandas.DataFrame()
        for column_name, series in dataframe.items():
            df_max = series.max()
            df_min = series.min()
            df_mean = series.mean()
            if df_max == df_min:
                normalized_df[column_name] = (series.copy()-df_mean)
                continue
            normalized_df[column_name] = (series-df_mean)/(df_max-df_min)

        return normalized_df

    @staticmethod
    def cost_function(X, y, theta, reg_parameter = 0.0):
        '''
        :param X: Data set of size mxn, containing n attributes in the columns and m observations
            in the rows
        :param y: Output's of the dataset of size mx(n_labels), containing m observations in the rows and
            n_labels labels in the columns.
        :param theta: 1D-array of size (n+1)*(n_labels) where n is the number of attributes and n_labels the number+
            of labeled outputs
        :param reg_parameter: Regularization parameter value
        :return: Value of the logistic regression cost function
        Note: Inputs, except for the reg parameter, must be numpy arrays.
        '''

        m = X.shape[0]
        n = X.shape[1]
        n_labels = y.shape[1] if len(y.shape) == 2 else 1
        theta = theta.reshape(n+1, n_labels)
        #Adds the bias term
        X = numpy.column_stack((numpy.ones(m), X))
        h = NeuralNetwork.sigmoid_function(X.dot(theta)).flatten()
        y = y.flatten()
        cost_fun = -y.dot(numpy.log(h))-(1.0-y).dot(numpy.log(1.0-h))
        cost_fun = (1.0/m)*cost_fun + (0.5*reg_parameter/m)*(theta[:,1:].flatten().dot(theta[:,1:].flatten()))

        return cost_fun

    @staticmethod
    def cost_function_grad(X, y, theta, reg_parameter = 0.0):
        '''
        :param X: Data set of size mxn, containing n attributes in the columns and m observations
            in the rows
        :param y: Output's of the dataset of size mx(n_labels), containing m observations in the rows and
            n_labels labels in the columns.
        :param theta: 1D-array of size (n+1)*(n_labels) where n is the number of attributes and n_labels the number+
            of labeled outputs
        :param reg_parameter: Regularization parameter value
        :return: 1D-array of size (n+1)*(n_labels) Value of the logistic regression gradient function
        Note: Inputs, except for the reg parameter, must be numpy arrays.
        '''
        m = X.shape[0]
        n = X.shape[1]
        n_labels = y.shape[1] if len(y.shape) == 2 else 1
        theta = theta.reshape(n + 1, n_labels)

        X = numpy.column_stack((numpy.ones(m), X))
        h = NeuralNetwork.sigmoid_function(X.dot(theta))
        #error = numpy.sum((h-y), axis=1)
        error = h-y
        aux_theta = numpy.column_stack((numpy.zeros(n+1), theta[:, 1:]))
        grad = (1.0/m)*(X.T.dot(error)+reg_parameter*aux_theta)

        return grad.flatten()

    @staticmethod
    def sigmoid_function(z):
        return 1.0/(1.0+numpy.exp(-z))

