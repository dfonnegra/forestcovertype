import os
from data_set_analysis.DataDepuration import DataDepuration
from neural_network.NeuralNetwork import NeuralNetwork
import random
import numpy


dataset_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) \
                   + "/data_set/covtype.data"
data_depuration = DataDepuration(dataset_location)
att_df = data_depuration.forest_attributes_data_frame()
out_df = data_depuration.forest_cover_type_series()

neural_net = NeuralNetwork(attributes_dataframe=att_df,
                           output_series=out_df, n_labels=7)
neural_net.train_nn()
print("Accuracy of neural network = {0:.1f}%".
      format(100*neural_net.nn_accuracy(neural_net.test_set.as_matrix(), neural_net.output_test.as_matrix())))

while True:
    file_name = input("Please insert the file location of the data_set you would like to determine"
                      "the cover type,\nif you want to stop predicting write exit"
                      "\nif you want to see a sample result please write sample:\n")
    if file_name == "exit": break
    if file_name == "sample":
        sample_indexes = list(range(neural_net.test_set.shape[0]))
        random.shuffle(sample_indexes)
        att_df = neural_net.test_set.iloc[sample_indexes[0:30]]
        out_ser = numpy.argmax(neural_net.output_test.iloc[sample_indexes[0:30]].as_matrix(), axis=1)+1.0

    else:
        try:
            data_depuration = DataDepuration(file_name)
            att_df = NeuralNetwork.normalize_data(data_depuration.forest_attributes_data_frame())
            out_ser = data_depuration.forest_cover_type_series()
        except IOError:
            print("File not found or can't access to file.")
            continue

    prediction = neural_net.nn_prediction(att_df.as_matrix())
    if out_ser is None:
        for i in range(len(prediction)):
            print("Prediction line {0}:".format(i), prediction[i])
    else:
        for i in range(len(prediction)):
            print("Real:", out_ser[i], "Prediction:", prediction[i])
        print("Accuracy: {0:.1f}%".format(100 * neural_net.nn_accuracy(att_df.as_matrix(), out_ser)))
