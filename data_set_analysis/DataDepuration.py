import os
import pandas


class DataDepuration:

    def __init__(self, data_set_location = None):
        if data_set_location is None:
            data_set_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) \
                                + "/data_set/covtype.data"
        self.data_set_location = data_set_location
        self.__forest_cover_dataframe = None
        self.__nemo_list = None

    @property
    def forest_cover_dataframe(self):
        if self.__forest_cover_dataframe is None:
            print("Depurating data...")
            data_file = open(self.data_set_location)
            data_set = list()
            for line in data_file:
                data_line = list()
                for data in line.strip().split(','):
                    data_line.append(float(data))
                data_set.append(data_line)
            data_frame = pandas.DataFrame(data=data_set, columns=self.nemo_list[0:len(data_set[0])])
            self.__forest_cover_dataframe = data_frame
            print("Depuration finished...")
        return self.__forest_cover_dataframe

    def forest_cover_type_series(self):
        if "Cover Type" in self.forest_cover_dataframe.keys():
            return self.forest_cover_dataframe["Cover Type"]
        return None

    def forest_attributes_data_frame(self):
        if "Cover Type" in self.forest_cover_dataframe.keys():
            return self.forest_cover_dataframe.drop("Cover Type", 1)
        return self.forest_cover_dataframe

    @property
    def nemo_list(self):
        if self.__nemo_list is None:
            nemo_list = list()
            nemo_list.append("Elevation")
            nemo_list.append("Aspect")
            nemo_list.append("Slope")
            nemo_list.append("Horizontal distance to Hydrology")
            nemo_list.append("Vertical distance to Hydrology")
            nemo_list.append("Horizontal distance to Roadways")
            nemo_list.append("Hillshade 9am")
            nemo_list.append("Hillshade Noon")
            nemo_list.append("Hillshade 3pm")
            nemo_list.append("Horizontal distance to Fire Points")
            for i in range(4):
                nemo_list.append("Wilderness Area " + str(i + 1))
            for i in range(40):
                nemo_list.append("Soil Type " + str(i + 1))
            nemo_list.append("Cover Type")
            self.__nemo_list = nemo_list
        return self.__nemo_list
