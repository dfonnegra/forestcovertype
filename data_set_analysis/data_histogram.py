import os
from data_set_analysis.DataDepuration import DataDepuration
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

data_depuration = DataDepuration()

fig_count = 1
#With this histograms it is shown the distribution of values over the real line.
#Most of them have gaussian like distributions.
for column_nemo, series in data_depuration.forest_cover_dataframe.items():
    if (column_nemo.startswith("Wilderness") or column_nemo.startswith("Soil")): continue
    print("Plotting", column_nemo, "Histogram")
    pyplot.figure(fig_count)
    pyplot.hist(x=series, histtype='bar', bins=30)
    pyplot.title(column_nemo)
    fig_count += 1

#Interesting but heavy plot... Shows that there are limits in Elevation and Aspect depending on the Cover Type
#plot3d = Axes3D(pyplot.figure(fig_count))
#plot3d.scatter(xs=data_depuration.forest_cover_dataframe["Elevation"],
#               ys=data_depuration.forest_cover_dataframe["Aspect"],
#               zs=data_depuration.forest_cover_dataframe["Cover Type"])

pyplot.show()

